﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    //TODO: Use class "LongDeposit" from previous task ("Aggregation").

    public class LongDeposit : Deposit, IProlongable
    {
        public LongDeposit(decimal amount, int period) : base(amount, period)
        {

        }

        public bool CanToProlong()
        {
            if (Period < 36)
                return true;
            return false;
        }

        public override decimal Income()
        {
            var income = Amount;
            for (int i = 6; i < Period; i++)
            {
                income += income * (decimal)0.15;
            }
            return income - Amount;
        }
    }
    //TODO: Implement interface "IProlongable" in class "LongDeposit".

    //Interface's method should return true if deposit term was no more than 3 years.

}
