﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    //TODO: Use class "Client" from previous task ("Aggregation").
    public class Client: IEnumerable<Deposit>
    {
        private readonly Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
        }

        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;
                    return true;
                }
            }
            return false;
        }

        public decimal TotalIncome()
        {
            decimal totalIncome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null)
                {
                    totalIncome += deposits[i].Income();
                }
            }
            return totalIncome;
        }

        public decimal MaxIncome()
        {
            decimal total = deposits[0].Income();

            foreach (Deposit depo in deposits)
            {
                if (depo != null && depo.Income() > total)
                {
                    total = depo.Income();
                }
            }
            return total;
        }

        public decimal GetIncomeByNumber(int number)
        {
            if (deposits[number - 1] != null)
                return deposits[number - 1].Income();
            else
                return 0;
        }

        public IEnumerator<Deposit> GetEnumerator()
        {
            foreach (Deposit name in deposits)
            {
                yield return name;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void SortDeposits()
        {
            Array.Sort(deposits);
        }
        public int CountPossibleToProlongDeposit()
        {
            int sum = 0;
            foreach (Deposit depo in deposits)
            {
                if (depo is SpecialDeposit && (depo as SpecialDeposit).CanToProlong())
                {
                    sum++;
                }
                if (depo is LongDeposit && (depo as LongDeposit).CanToProlong())
                {
                    sum++;
                }
            }
            return sum;
        }
    }

    //TODO: Implement interface "IEnumerable<Deposit>" in class "Client".

    //TODO: Create public method "SortDeposits" that sorts array of deposits by general sum of money in descending order.

    //TODO: Create public method "CountPossibleToProlongDeposit" that returns amount of current client's deposits that can be prolonged.

}
