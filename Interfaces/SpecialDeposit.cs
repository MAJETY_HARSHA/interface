﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    //TODO: Use class "SpecialDeposit" from previous task ("Aggregation").

    public class SpecialDeposit : Deposit, IProlongable
    {
        private static decimal percent = (decimal)0.01;
        public SpecialDeposit(decimal amount, int period) : base(amount, period)
        {

        }

        public bool CanToProlong()
        {
            if (Amount > 1000)
            {
                return true;
            }
            return false;
        }

        public override decimal Income()
        {
            var income = Amount;
            var incomePercent = percent;
            for (int i = 0; i < Period; i++)
            {
                income += income * incomePercent;
                incomePercent += percent;
            }
            return income - Amount;
        }
    }
    //TODO: Implement interface "IProlongable" in class "SpecialDeposit".

    //Interface's method should return true if deposited more than 1000 UAH.

}
