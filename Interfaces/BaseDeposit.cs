﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal amount, int period) : base(amount, period)
        {

        }
        public override decimal Income()
        {
            int month = 0;
            decimal increased = Amount;
            decimal income = 0;
            while (month < Period)
            {
                month++;
                increased += System.Math.Round((increased / 100 * 5), 2);
            }
            income = increased - Amount;
            return income;
        }
    }
}
