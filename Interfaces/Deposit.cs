﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Interfaces
{
    //TODO: Use class "Deposit" from previous task ("Aggregation").
    abstract public class Deposit : IComparable<Deposit>
    {
        private readonly decimal amount;
        private readonly int period;

        public decimal Amount { get { return amount; } }

        public int Period { get { return period; } }

        public Deposit(decimal depositAmount, int depositPeriod)
        {
            amount = depositAmount;
            period = depositPeriod;
        }

        public abstract decimal Income();
        public int CompareTo( Deposit other)
        {
            decimal thisamount = amount;
             return thisamount.CompareTo(other.amount );
        }
    }

    //TODO: Implement interface "IComparable<Deposit>" in abstract class "Deposit".

    //Use general sum of money (deposit amount plus interest on the deposit for the entire period) as a comparison criterion.

}
